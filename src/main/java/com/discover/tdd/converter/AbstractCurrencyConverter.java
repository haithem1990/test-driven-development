package com.discover.tdd.converter;

import com.discover.tdd.enums.Currency;
import com.discover.tdd.money.DefaultMoney;
import com.discover.tdd.money.Money;

public abstract class AbstractCurrencyConverter implements CurrencyConverter {

    @Override
    public DefaultMoney convert(Money money) {
        if (Currency.DEFAULT.getCurrencyName().equals(money.getCurrency().getCurrencyName()))
            return new DefaultMoney(money.getAmount());
        return new DefaultMoney(money.getAmount() * money.getCurrency().getCoefficient());
    }
}
