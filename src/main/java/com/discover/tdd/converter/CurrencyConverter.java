package com.discover.tdd.converter;

import com.discover.tdd.money.DefaultMoney;
import com.discover.tdd.money.Money;

public interface CurrencyConverter {

    DefaultMoney convert(Money money);

    Money unConvert(DefaultMoney defaultMoney);
}
