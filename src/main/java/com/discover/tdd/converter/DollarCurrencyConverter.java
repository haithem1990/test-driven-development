package com.discover.tdd.converter;

import com.discover.tdd.enums.Currency;
import com.discover.tdd.money.DefaultMoney;
import com.discover.tdd.money.Dinar;
import com.discover.tdd.money.Dollar;
import com.discover.tdd.money.Money;

public class DollarCurrencyConverter extends AbstractCurrencyConverter {

    private static CurrencyConverter currencyConverter;

    public static CurrencyConverter getInstance() {
        if (currencyConverter == null)
            currencyConverter = new DollarCurrencyConverter();
        return currencyConverter;
    }

    @Override
    public Money unConvert(DefaultMoney defaultMoney) {
        if (Currency.DEFAULT.getCurrencyName().equals(Currency.USD.getCurrencyName()))
            return new DefaultMoney(defaultMoney.getAmount());
        return new Dollar(defaultMoney.getAmount() / Currency.USD.getCoefficient());
    }
}
