package com.discover.tdd.converter;

import com.discover.tdd.enums.Currency;
import com.discover.tdd.money.DefaultMoney;
import com.discover.tdd.money.Dinar;
import com.discover.tdd.money.Franc;
import com.discover.tdd.money.Money;

public class FrancCurrencyConverter extends AbstractCurrencyConverter {

    private static CurrencyConverter currencyConverter;

    public static CurrencyConverter getInstance() {
        if (currencyConverter == null)
            currencyConverter = new FrancCurrencyConverter();
        return currencyConverter;
    }

    @Override
    public Money unConvert(DefaultMoney defaultMoney) {
        if (Currency.DEFAULT.getCurrencyName().equals(Currency.FHC.getCurrencyName()))
            return new DefaultMoney(defaultMoney.getAmount());
        return new Franc(defaultMoney.getAmount() / Currency.FHC.getCoefficient());
    }

}
