package com.discover.tdd.enums;

public enum Currency {
    DEFAULT("USD", 0),
    USD("USD", 6),
    FHC("FHC", 5),
    DINAR("DINAR", 2),
    ;
    private String currencyName;
    private double coefficient;

    Currency(String currencyName, double coefficient) {
        this.currencyName = currencyName;
        this.coefficient = coefficient;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }
}
