package com.discover.tdd.money;

import com.discover.tdd.enums.Currency;

public class DefaultMoney extends Money {

    private Currency defaultCurrency;

    public DefaultMoney(double amount) {
        super(amount);
        defaultCurrency = Currency.DEFAULT;
    }

    public DefaultMoney(double amount,Currency currency) {
        super(amount);
        defaultCurrency = currency;
    }

    @Override
    public Currency getCurrency() {
        return getDefaultCurrency();
    }

    public Currency getDefaultCurrency() {
        return defaultCurrency;
    }
}
