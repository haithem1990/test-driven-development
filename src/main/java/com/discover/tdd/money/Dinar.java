package com.discover.tdd.money;

import com.discover.tdd.enums.Currency;

public class Dinar extends Money {

    public Dinar(double amount) {
        super(amount);
    }

    @Override
    public Currency getCurrency() {
        return Currency.DINAR;
    }
}
