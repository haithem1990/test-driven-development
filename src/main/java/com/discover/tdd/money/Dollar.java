package com.discover.tdd.money;

import com.discover.tdd.enums.Currency;

public class Dollar extends Money {
    public Dollar(double amount) {
        super(amount);
    }

    @Override
    public Currency getCurrency() {
        return Currency.USD;
    }

}
