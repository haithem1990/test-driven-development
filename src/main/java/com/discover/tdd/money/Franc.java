package com.discover.tdd.money;

import com.discover.tdd.enums.Currency;

public class Franc extends Money {

    public Franc(double amount) {
        super(amount);
    }

    @Override
    public Currency getCurrency() {
        return Currency.FHC;
    }
}
