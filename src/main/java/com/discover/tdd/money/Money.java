package com.discover.tdd.money;

import com.discover.tdd.enums.Currency;

public abstract class Money {

    protected double amount;

    public Money(double amount) {
        this.amount = amount;
    }

    public void multiply(double multiplicationFactor) {
        this.amount *= multiplicationFactor;
    }

    public double getAmount() {
        return this.amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public abstract Currency getCurrency();
}
