package com.discover.tdd.test;

import com.discover.tdd.converter.CurrencyConverter;
import com.discover.tdd.converter.DinarCurrencyConverter;
import com.discover.tdd.enums.Currency;
import com.discover.tdd.money.DefaultMoney;
import com.discover.tdd.money.Dinar;
import com.discover.tdd.money.Money;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DinarCurrencyConverterTest {
    @Test
    public void convertDinarToDefaultMoney() {
        if (Currency.DEFAULT.getCurrencyName().equals(Currency.DINAR.getCurrencyName())) {
            Money dinarMoney = new Dinar(5);
            CurrencyConverter dinarCurrencyConverter = DinarCurrencyConverter.getInstance();
            DefaultMoney convertedDinarMoney = dinarCurrencyConverter.convert(dinarMoney);
            assertEquals(Currency.DINAR.getCoefficient() * dinarMoney.getAmount(), convertedDinarMoney.getAmount());
            assertEquals(Currency.DEFAULT.getCurrencyName(), convertedDinarMoney.getCurrency().getCurrencyName());
        }
    }

    @Test
    public void convertDinarToDinarDefaultMoney() {

        if (Currency.DEFAULT.getCurrencyName().equals(Currency.DINAR.getCurrencyName())) {

            Money dinarMoney = new Dinar(5);

            CurrencyConverter dinarCurrencyConverter = DinarCurrencyConverter.getInstance();

            DefaultMoney convertedDinarMoney = dinarCurrencyConverter.convert(dinarMoney);

            assertEquals(5, convertedDinarMoney.getAmount());
            assertEquals(Currency.DEFAULT.getCurrencyName(), convertedDinarMoney.getCurrency().getCurrencyName());
        }
    }

    @Test
    public void testUnConvertDinarFromDefaultMoney() {
        if (!Currency.DEFAULT.getCurrencyName().equals(Currency.DINAR.getCurrencyName())) {
            DefaultMoney defaultMoney = new DefaultMoney(6);
            CurrencyConverter dinarCurrencyConverter = DinarCurrencyConverter.getInstance();
            Money dinarMoney = dinarCurrencyConverter.unConvert(defaultMoney);
            assertEquals(6/Currency.DINAR.getCoefficient(), dinarMoney.getAmount());
            assertEquals(Currency.DINAR.getCurrencyName(), dinarMoney.getCurrency().getCurrencyName());
        }
    }

    @Test
    public void testUnConvertDinarFromDinarDefaultMoney() {
        if (Currency.DEFAULT.getCurrencyName().equals(Currency.DINAR.getCurrencyName())) {
            DefaultMoney defaultMoney = new DefaultMoney(6);
            CurrencyConverter dinarCurrencyConverter = DinarCurrencyConverter.getInstance();
            Money dinarMoney = dinarCurrencyConverter.unConvert(defaultMoney);
            assertEquals(6, dinarMoney.getAmount());
            assertEquals(Currency.DINAR, dinarMoney.getCurrency());
        }
    }
}
