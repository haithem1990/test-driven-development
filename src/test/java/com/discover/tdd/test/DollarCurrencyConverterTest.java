package com.discover.tdd.test;

import com.discover.tdd.converter.CurrencyConverter;
import com.discover.tdd.converter.DollarCurrencyConverter;
import com.discover.tdd.enums.Currency;
import com.discover.tdd.money.DefaultMoney;
import com.discover.tdd.money.Dollar;
import com.discover.tdd.money.Money;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DollarCurrencyConverterTest {
    @Test
    public void convertDollarToDefaultMoney() {
        if (!Currency.DEFAULT.getCurrencyName().equals(Currency.USD.getCurrencyName())) {
            Money dollarMoney = new Dollar(5);
            CurrencyConverter dollarCurrencyConverter = DollarCurrencyConverter.getInstance();
            DefaultMoney convertedDollarMoney = dollarCurrencyConverter.convert(dollarMoney);
            assertEquals(30, convertedDollarMoney.getAmount());
            assertEquals(Currency.DEFAULT.getCurrencyName(), convertedDollarMoney.getCurrency().getCurrencyName());
        }
    }

    @Test
    public void convertDollarToDollarDefaultMoney() {

        if (Currency.DEFAULT.getCurrencyName().equals(Currency.USD.getCurrencyName())) {

            Money dollarMoney = new Dollar(5);

            CurrencyConverter dollarCurrencyConverter = DollarCurrencyConverter.getInstance();

            DefaultMoney convertedDollarMoney = dollarCurrencyConverter.convert(dollarMoney);

            assertEquals(5, convertedDollarMoney.getAmount());
            assertEquals(Currency.DEFAULT.getCurrencyName(), convertedDollarMoney.getCurrency().getCurrencyName());
        }
    }

    @Test
    public void testUnConvertDollarFromDefaultMoney() {
        if (!Currency.DEFAULT.getCurrencyName().equals(Currency.USD.getCurrencyName())) {
            DefaultMoney defaultMoney = new DefaultMoney(6);
            CurrencyConverter dollarCurrencyConverter = DollarCurrencyConverter.getInstance();
            Money dollarMoney = dollarCurrencyConverter.unConvert(defaultMoney);
            assertEquals(6 / Currency.USD.getCoefficient(), dollarMoney.getAmount());
            assertEquals(Currency.USD.getCurrencyName(), dollarMoney.getCurrency().getCurrencyName());
        }
    }

    @Test
    public void testUnConvertDollarFromDollarDefaultMoney() {
        if (!Currency.DEFAULT.getCurrencyName().equals(Currency.USD.getCurrencyName())) {
            DefaultMoney defaultMoney = new DefaultMoney(6);
            CurrencyConverter dollarCurrencyConverter = DollarCurrencyConverter.getInstance();
            Money dollarMoney = dollarCurrencyConverter.unConvert(defaultMoney);
            assertEquals(6, dollarMoney.getAmount());
            assertEquals(Currency.USD, dollarMoney.getCurrency());
        }
    }
}
