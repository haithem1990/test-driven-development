package com.discover.tdd.test;

import com.discover.tdd.converter.CurrencyConverter;
import com.discover.tdd.converter.FrancCurrencyConverter;
import com.discover.tdd.enums.Currency;
import com.discover.tdd.money.DefaultMoney;
import com.discover.tdd.money.Franc;
import com.discover.tdd.money.Money;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FrancCurrencyConverterTest {

    @Test
    public void convertFrancToDefaultMoney() {
        if (!Currency.DEFAULT.getCurrencyName().equals(Currency.FHC.getCurrencyName())) {
            Money francMoney = new Franc(5);
            CurrencyConverter francCurrencyConverter = FrancCurrencyConverter.getInstance();
            DefaultMoney convertedFrancMoney = francCurrencyConverter.convert(francMoney);
            assertEquals(Currency.FHC.getCoefficient() * francMoney.getAmount(), convertedFrancMoney.getAmount());
            assertEquals(Currency.DEFAULT.getCurrencyName(), convertedFrancMoney.getCurrency().getCurrencyName());
        }
    }

    @Test
    public void convertFrancToFrancDefaultMoney() {

        if (Currency.DEFAULT.getCurrencyName().equals(Currency.FHC.getCurrencyName())) {

            Money francMoney = new Franc(5);

            CurrencyConverter francCurrencyConverter = FrancCurrencyConverter.getInstance();

            DefaultMoney convertedFrancMoney = francCurrencyConverter.convert(francMoney);

            assertEquals(5, convertedFrancMoney.getAmount());
            assertEquals(Currency.DEFAULT.getCurrencyName(), convertedFrancMoney.getCurrency().getCurrencyName());
        }
    }

    @Test
    public void testUnConvertFrancFromDefaultMoney() {
        if (!Currency.DEFAULT.getCurrencyName().equals(Currency.FHC.getCurrencyName())) {
            DefaultMoney defaultMoney = new DefaultMoney(6);
            CurrencyConverter francCurrencyConverter = FrancCurrencyConverter.getInstance();
            Money francMoney = francCurrencyConverter.unConvert(defaultMoney);
            assertEquals(defaultMoney.getAmount() / Currency.FHC.getCoefficient(), francMoney.getAmount());
            assertEquals(Currency.FHC.getCurrencyName(), francMoney.getCurrency().getCurrencyName());
        }
    }

    @Test
    public void testUnConvertFrancFromFrancDefaultMoney() {
        if (Currency.DEFAULT.getCurrencyName().equals(Currency.FHC.getCurrencyName())) {
            DefaultMoney defaultMoney = new DefaultMoney(6);
            CurrencyConverter francCurrencyConverter = FrancCurrencyConverter.getInstance();
            Money francMoney = francCurrencyConverter.unConvert(defaultMoney);
            assertEquals(francMoney.getAmount(), francMoney.getAmount());
            assertEquals(Currency.FHC, francMoney.getCurrency());
        }
    }
}