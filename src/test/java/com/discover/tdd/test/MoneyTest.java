package com.discover.tdd.test;

import com.discover.tdd.enums.Currency;
import com.discover.tdd.money.Dollar;
import com.discover.tdd.money.Franc;
import com.discover.tdd.money.Money;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MoneyTest {

    @Test
    public void multiply() {
        // initialize
        Money dollarMoney = new Dollar(5);
        // invoke
        dollarMoney.multiply(2);
        // check result
        assertEquals(10, dollarMoney.getAmount());
    }

    @Test
    public void getCurrency() {
        // initialize
        Money dollarMoney = new Dollar(5);
        Money francMoney = new Franc(5);
        // invoke
        Currency dollarCurrency = dollarMoney.getCurrency();
        Currency francCurrency = francMoney.getCurrency();
        // check
        assertEquals(Currency.USD, dollarCurrency);
        assertEquals(Currency.FHC, francCurrency);
    }
}
